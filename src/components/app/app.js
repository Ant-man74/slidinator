import React from 'react';
import "../../libs/template.scss";
function App() {
  return (
    <div className="container">
      <div className="main-content">
        Main
      </div>
      <div className="sidebar">
        Details
      </div>
      <div className="navigator">
        Navigator
      </div>
    </div>
  );
}

export default App;
